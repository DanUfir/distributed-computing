import java.net.*;
import java.io.*;

/**
 * A subclass of DatagramSocket which contains 
 * methods for sending and receiving messages
 * @author M. L. Liu
 */

public class MyDatagramSocket extends DatagramSocket {
static final int MAX_LEN = 100; 

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    boolean loggedIn = false;
    String username;
    
    MyServerDatagramSocket(int portNo) throws SocketException{
     super(portNo);
   }
   public void sendMessage(InetAddress receiverHost,
                           int receiverPort,
                           String message)
   		          throws IOException {	
         byte[ ] sendBuffer = message.getBytes( );                                     
         DatagramPacket datagram = 
            new DatagramPacket(sendBuffer, sendBuffer.length, 
                                  receiverHost, receiverPort);
         this.send(datagram);
   } // end sendMessage

   public String receiveMessage( )
		throws IOException {		
         byte[ ] receiveBuffer = new byte[MAX_LEN];
         DatagramPacket datagram =
            new DatagramPacket(receiveBuffer, MAX_LEN);
         this.receive(datagram);
		 String message = new String(receiveBuffer);
         if(message.contains("login"))
		 {
			 Login(message);
		 }
		 return message;
   } //end receiveMessage

   public DatagramMessage receiveMessageAndSender( )
		throws IOException {		
         byte[ ] receiveBuffer = new byte[MAX_LEN];
         DatagramPacket datagram =
            new DatagramPacket(receiveBuffer, MAX_LEN);
         this.receive(datagram);
         // create a DatagramMessage object, to contain message
         //   received and sender's address
         DatagramMessage returnVal = new DatagramMessage( );
         returnVal.putVal(new String(receiveBuffer),
                          datagram.getAddress( ),
                          datagram.getPort( ));
         return returnVal;
   } //end receiveMessage
   
   public String Login(String login)
   {
       File f = new File("C:/server"+login);
       if (f.exists()) 
        {  
            setUsername(login);
            setLoggedIn(true);
            return "911 - Successfully logged in.";
        }
       else if(login.equals(""))
       {
           return "913 – Name of account not specified";
       }
       else 
       {
           return "912 – Account not found";
       }
   }
   
   public String Create(String username)
   {
       File f = new File("C:/server/"+username);
       if (f.exists()) 
            {
                return "932 – Account already exists";   
            }
       else if(username.equals(""))
       {
           return "933 – Name of account not specified";
       }
       else
       {
           f.mkdir();
           return "931 – Account successfully created";
       }
   }
   
   public String Help()
   {
       return   "Login – Logs into the given account\n" +
                "Create – Creates an account\n" +
                "Display – Displays the contents of the account\n" +
                "Upload – Upload a file specified\n" +
                "Download – Download a file specified\n" +
                "Logout – Logs the user out of the account";      
   }
   
    public void Download(String name) throws IOException    
   {
       File f = new File("C:/server/"+name);
       if(!f.exists())
       {
           System.out.println("952 – File specified not found");
       }
       else if(name.equals(""))
       {
           System.out.println("953 – No file specified");
       }
       else
       {
           
           String home = System.getProperty("user.home");
           File destination = new File(home+"/Downloads/"+name);
           FileInputStream fs = null;
           FileOutputStream os = null;
           try   
           {
               fs = new FileInputStream(f);
               os = new FileOutputStream(destination);
               byte[] buffer = new byte[1024];
               int length;
               while ((length = fs.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
               System.out.println(" 951 – File Successfully downloaded");
            } 
           catch(Exception e)
           {
               
           }
           finally 
           {
                fs.close();
                os.close();
            }
       }
   }
   
   public void Upload(String path)
   {
       
       File file  = new File(path);
       
       try
       {
           file.createNewFile();
           if(file.exists())
           {
               System.out.println("941 – File Successfully uploaded");
           }

       }
       catch (Exception x)
        {
            
        }
   }
   
   public String Logout()
   {
       return "961 – Successfully logged out";
   }
   
} //end class
