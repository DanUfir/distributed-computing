import java.net.*;

public class ReceiveCommand{
    
   private String command;
   private InetAddress senderAddress;
   private int senderPort;
   
   public void putVal(String command, InetAddress addr, int port) {
      this.command = command;
      this.senderAddress = addr;
      this.senderPort = port;
   }

   public String getCommand() {
        return command;
    }
   public InetAddress getAddress( ) {
      return this.senderAddress;
   }

   public int getPort( ) {
      return this.senderPort;
   }
} // end class  
