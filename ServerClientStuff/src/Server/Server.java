import java.io.*;

/**
 * This module contains the application logic of an echo server
 * which uses a connectionless datagram socket for interprocess 
 * communication.
 * A command-line argument is required to specify the server port.
 * @author M. L. Liu
 */

public class Server {
   public static void main(String[] args) {
      int serverPort = 7;    // default port
      if (args.length == 1 )
         serverPort = Integer.parseInt(args[0]);       
      try {
         
   	   MyServerDatagramSocket mySocket = new MyServerDatagramSocket(serverPort); 
          System.out.println("Welcome to the server");  
       
          while (true) {  
              
            ReceiveCommand request = mySocket.receiveCommandAndSender();
            
            String message = request.getCommand();
            System.out.println("Command received:" + message);
            
            System.out.println(message);
            
            if(message.trim().contains("help"))
            {
                message = mySocket.Help(); 
            }
            
            if(message.trim().contains("login"))
            {              
                String[] username = message.split(" ");
                message = mySocket.Login(username[1].trim());
            }
            if(!mySocket.loggedIn)
            {
                if(message.trim().contains("download") || message.trim().contains("upload") || message.trim().contains("logout"))
                {
                    message = mySocket.NotLogedIn();
                }
            }
            else if(message.trim().contains("download"))
            {              
                String[] nameOfFile = message.split(" ");
                System.out.println(nameOfFile[1]);
                mySocket.Download(nameOfFile[1].trim());
            }
            else if(message.trim().contains("upload"))
            {              
                String[] nameOfFile = message.split(" ");
                System.out.println(nameOfFile[1]);
                mySocket.Download(nameOfFile[1].trim());
            }
            else if(message.trim().contains("logout"))
            {
                message = mySocket.Logout();
            }
            
            mySocket.sendResponse(request.getAddress( ), request.getPort( ), message);
		   } //end while
       } // end try
	    catch (Exception ex) {
          ex.printStackTrace( );
	    } // end catch
   } //end main
   
   
} // end class      
