import java.net.*;
import java.io.*;

/**
 * This class is a module which provides the application logic
 * for an Echo client using connectionless datagram socket.
 * @author M. L. Liu
 */
public class ClientHelper {
   private MyDatagramSocket mySocket;
   private InetAddress serverHost;
   private int serverPort;
   
    boolean loggedIn = false;
    String username;
    
    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
   
    

   ClientHelper(String hostName, String portNum) 
      throws SocketException, UnknownHostException { 
  	   this.serverHost = InetAddress.getByName(hostName);
  		this.serverPort = Integer.parseInt(portNum);
      // instantiates a datagram socket for both sending
      // and receiving data
   	this.mySocket = new MyDatagramSocket(); 
   } 
	
   public String getEcho( String message) 
      throws SocketException, IOException {                                                                                 
      String echo = "";    
      mySocket.sendMessage( serverHost, serverPort, message);
	   // now receive the echo
      echo = mySocket.receiveMessage();
      return echo;
   } //end getEcho
   
   public void download()
   {
        
     //   mySocket.();
   }
     public String Login(String login)
    {
       File f = new File("C:/server/"+login);
       if (f.exists()) 
        {  
            setUsername(login);
            setLoggedIn(true);
            return "911 - Successfully logged in.";
        }
       else if(login.equals(""))
       {
           return "913 � Name of account not specified";
       }
       else 
       {
           return "912 � Account not found";
       }
   }
   
   public String Create(String username){
        File f = new File("C:/server/"+username);
       
        if (f.exists()){
            return "932 � Account already exists";   
        }//end if
        else if(username.equals("")){
            return "933 � Name of account not specified";
        }//end else if
        else{
            f.mkdir();
            return "931 � Account successfully created";
        }//end else
   }//end Create
   
   public String Help()
   {
       return   "Login � Logs into the given account\n" +
                "Create � Creates an account\n" +
                "Display � Displays the contents of the account\n" +
                "Upload � Upload a file specified\n" +
                "Download � Download a file specified\n" +
                "Logout � Logs the user out of the account";      
   }
   
    public void Download(String name) throws IOException    
   {
       File f = new File("C:/server/"+getUsername()+"/"+name);
       if(!f.exists())
       {
           System.out.println("952 � File specified not found");
       }
       else if(name.equals(""))
       {
           System.out.println("953 � No file specified");
       }
       else
       {
           String home = System.getProperty("user.home");
           File destination = new File(home+"/Downloads/"+name);
           FileInputStream fs = null;
           FileOutputStream os = null;
           try   
           {
               fs = new FileInputStream(f);
               os = new FileOutputStream(destination);
               byte[] buffer = new byte[1024];
               int length;
               while ((length = fs.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
               System.out.println("951 � File Successfully downloaded");
            } 
           catch(Exception e)
           {
               
           }
           finally 
           {
                fs.close();
                os.close();
            }
       }
   }
    
   public String NotLogedIn()
   {
       return "971 � You must be logged in to use these commands. For help type help.";
   }
   
   public String Logout()
   {
       setUsername("");
       return "961 � Successfully logged out";
   }
   
   
    public void upload(InetAddress receiverHost, int receiverPort, String filePath) throws IOException {	          
          try{
                byte[ ] sendBuffer = new byte[8124];
                
                File destination = new File(filePath);
                FileInputStream fs = null;
                DatagramPacket data = new DatagramPacket(sendBuffer, 1024);
              
                String message;
                if(!destination.exists())
                {
                    message = ("942 – File specified not found");
                    message = ("941 – File Successfully uploaded");
                    sendBuffer = message.getBytes();
                }
                else if(filePath.equals("") || filePath.equals(null))
                {
                    message = ("943 – No file specified");
                    sendBuffer = message.getBytes();
                }
                else
                {
                     int leng = (int)destination.length();
                     sendBuffer = new byte[leng];    

                     fs = new FileInputStream(destination);
                     int amountRead = 0;
                     int n;
                     do{
                         n = fs.read(sendBuffer, amountRead, leng-amountRead);
                         amountRead += n;
                     }
                     while((amountRead < leng) && (n != -1));
                }    
                     DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
                     this.send(datagram);
                  } 
                 catch(Exception e)
                 {

                 }
       } 
      public void receiveDownload(String fileName) throws IOException    
     {
       try
       {
           String home = System.getProperty("user.home");
           File destination = new File(home+"/Downloads/"+fileName);
           byte [] sendBuffer = new byte[8124];
           DatagramPacket datagram = new DatagramPacket(sendBuffer, sendBuffer.length);
           
           this.receive(datagram);
           
           FileOutputStream fos = new FileOutputStream(destination);
           fos.write(sendBuffer);
       }
           catch(Exception e)
           {
               e.printStackTrace();
           }
       
   }
      
      public void downloadFile(InetAddress receiverHost, int receiverPort, String filename) throws IOException {	
            try{
                byte[ ] sendBuffer = new byte[8124];
                DatagramSocket datagram = new DatagramSocket(receiverPort);
                
                String home = System.getProperty("user.home");
                File destination = new File(home+"/Downloads/"+filename);
                FileOutputStream fs = new FileOutputStream(destination);
                
                DatagramPacket data = new DatagramPacket(sendBuffer, 1024);
                
                int amountRead = 0;
                while(true)
                {
                    datagram.receive(data);
                    amountRead ++;
                    fs.write(sendBuffer);
                    
                    if(amountRead >= 100)
                    {
                        break;
                    }
                /*String message;
                
                int leng = (int)f.length();
                sendBuffer = new byte[leng];    

                     fs = new FileInputStream(f);
                     int amountRead = 0;
                     int n;
                     do{
                         n = fs.read(sendBuffer, amountRead, leng-amountRead);
                         amountRead += n;
                     }
                     while((amountRead < leng) && (n != -1));

                     //dSocket.close();
                     //this.send(datagram);
                }    
                     DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
                   //  DatagramSocket dSocket = new DatagramSocket();
                     this.send(datagram);
                } 
            }
                 catch(Exception e)
                 {

                 }

}

   public void done( ) throws SocketException {
      mySocket.close( );
   }  //end done

} //end class
