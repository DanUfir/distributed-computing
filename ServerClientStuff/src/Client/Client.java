import java.io.*;

/**
 * This module contains the presentaton logic of an Echo Client.
 * @author M. L. Liu
 */
public class Client {
   static final String endMessage = ".";
   public static void main(String[] args) {
      InputStreamReader is = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(is);
      try {
         System.out.println("Welcome to the Servert.\n" +
                            "What is the name of the server host?");
         String hostName = br.readLine();
         if (hostName.length() == 0) // if user did not enter a name
            hostName = "localhost";  //   use the default host name
         
         System.out.println("What is the port number of the server host?");
         String portNum = br.readLine();
         if (portNum.length() == 0)
            portNum = "7";          // default port number
         
         ClientHelper helper = new ClientHelper(hostName, portNum);         
         boolean done = false;
         boolean logedIn = false;
         String username = "";
         String message, echo;
         while (!done) {
             System.out.println("For list of commands type help.");
            message = br.readLine( );
            if ((message.trim()).equals (endMessage)){
               done = true;
               helper.done( );
            }
               
            if(message.trim().contains("help"))
            {
                message = mySocket.Help(); 
            }
            
            if(message.trim().contains("login"))
            {              
                String[] user = message.split(" ");
                message = mySocket.Login(user[1].trim());
            }
            if(!mySocket.loggedIn)
            {
                if(message.trim().contains("download") || message.trim().contains("upload") || message.trim().contains("logout"))
                {
                    message = mySocket.NotLogedIn();
                }
            }
            else if(message.trim().contains("download"))
            {              
                String[] nameOfFile = message.split(" ");
                System.out.println(nameOfFile[1]);
                mySocket.Download(nameOfFile[1].trim());
            }
            else if(message.trim().contains("upload"))
            {              
                String[] nameOfFile = message.split(" ");
                System.out.println(nameOfFile[1]);
                mySocket.Download(nameOfFile[1].trim());
            }
            else if(message.trim().contains("logout"))
            {
                message = mySocket.Logout();
            }
            else {
               echo = helper.getEcho( message);
               System.out.println(echo);
            }
          } // end while
      } // end try  
      catch (Exception ex) {
         ex.printStackTrace( );
      } // end catch
   } //end main
} // end class      
