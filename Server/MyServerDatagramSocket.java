import java.net.*;
import java.io.*;

/**
 * A subclass of DatagramSocket which contains 
 * methods for sending and receiving messages
 * @author M. L. Liu
 */

public class MyServerDatagramSocket extends DatagramSocket {
static final int MAX_LEN = 100; 

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    boolean loggedIn = false;
    String username;
    
    MyServerDatagramSocket(int portNo) throws SocketException{
     super(portNo);
   }
   public void sendMessage(InetAddress receiverHost,
                           int receiverPort,
                           String message)
   		          throws IOException {	
         byte[ ] sendBuffer = message.getBytes( );                                     
         DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
         this.send(datagram);
   } // end sendMessage

   public String receiveMessage( )
		throws IOException {		
         byte[ ] receiveBuffer = new byte[MAX_LEN];
         DatagramPacket datagram =
            new DatagramPacket(receiveBuffer, MAX_LEN);
         this.receive(datagram);
		 String message = new String(receiveBuffer);
                 return message;
   } //end receiveMessage

   public DatagramMessage receiveMessageAndSender( )
		throws IOException {		
         byte[ ] receiveBuffer = new byte[MAX_LEN];
         DatagramPacket datagram =
            new DatagramPacket(receiveBuffer, MAX_LEN);
         this.receive(datagram);
         // create a DatagramMessage object, to contain message
         //   received and sender's address
         DatagramMessage returnVal = new DatagramMessage( );
         returnVal.putVal(new String(receiveBuffer),
                          datagram.getAddress( ),
                          datagram.getPort( ));
   		 return returnVal;
   } //end receiveMessage
   
   public void Login(InetAddress receiverHost, int receiverPort, String login) throws IOException
   {
       File f = new File("C:/server"+login);
       String message;
       if (f.exists()) 
        {  
            setUsername(login);
            setLoggedIn(true);
            message = "911 - Successfully logged in.";
        }
       else if(login.equals(""))
       {
           message = "913 � Name of account not specified";
       }
       else 
       {
           message =  "912 � Account not found";
       }
       byte [] sendBuffer = message.getBytes();
       DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
       this.send(datagram);
   }
   
   
   public void Create(InetAddress receiverHost, int receiverPort, String username) throws IOException
   {
      File f = new File("C:\\server\\"+username.toString());
      String message;
      
       if (f.exists()) 
        {
            message =  "932 � Account already exists";   
        }
       else if(username.equals(""))
       {
           message =  "933 � Name of account not specified";
       }
       else
       {
           f.mkdir();		  
           message = "931 � Account successfully created";
       }
       byte [] sendBuffer = message.getBytes();
       DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
       this.send(datagram);
   }
    
   
   public void Help(InetAddress receiverHost, int receiverPort) throws IOException
   {
       String message =     "Login � Logs into the given account\n" +
                            "Create � Creates an account\n" +
                            "Display � Displays the contents of the account\n" +
                            "Upload � Upload a file specified\n" +
                            "Download � Download a file specified\n" +
                            "Logout � Logs the user out of the account";  
       byte [] sendBuffer = message.getBytes();
       DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
       this.send(datagram);
       
   }
   
    public void ReceiveUpload(String fileName) throws IOException    
    {
       try
       {
           File f = new File("C:/server/"+getUsername()+"/"+fileName);
           byte [] sendBuffer = new byte[8124];
           DatagramPacket datagram = new DatagramPacket(sendBuffer, sendBuffer.length);
           
           this.receive(datagram);
           
           FileOutputStream fos = new FileOutputStream(f);
           fos.write(sendBuffer);
       }
           catch(Exception e)
           {
               e.printStackTrace();
           }
       
   }
    
    public void ReceiveDownload(String fileName) throws IOException    
    {
       try
       {
           String home = System.getProperty("user.home");
           File destination = new File(home+"/Downloads/"+fileName);
           byte [] sendBuffer = new byte[8124];
           DatagramPacket datagram = new DatagramPacket(sendBuffer, sendBuffer.length);
           
           this.receive(datagram);
           
           FileOutputStream fos = new FileOutputStream(f);
           fos.write(sendBuffer);
       }
           catch(Exception e)
           {
               e.printStackTrace();
           }
       
   }
      public void downloadFile(InetAddress receiverHost, int receiverPort, String filename) throws IOException {	          
          try{
                FileInputStream fs = null;
                byte[ ] sendBuffer;
                String message;
                File f = new File("C:/server/"+filename);
                if(!f.exists())
                {
                    message = ("952 � File specified not found");
                    sendBuffer = message.getBytes();
                }
                else if(filename.equals("") || filename.equals(null))
                {
                    message = ("953 � No file specified");
                    sendBuffer = message.getBytes();
                }
                else
                {
                     File file  = new File("C:\\server\\"+getUsername()+"\\"+filename);
                     int leng = (int)file.length();
                     sendBuffer = new byte[leng];    

                     fs = new FileInputStream(file);
                     int amountRead = 0;
                     int n;
                     do{
                         n = fs.read(sendBuffer, amountRead, leng-amountRead);
                         amountRead += n;
                     }
                     while((amountRead < leng) && (n != -1));

                     //dSocket.close();
                     //this.send(datagram);
                }    
                     DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
                   //  DatagramSocket dSocket = new DatagramSocket();
                     this.send(datagram);
                  } 
                 catch(Exception e)
                 {
                      e.printStackTrace();
                 }
       } 

       public void Upload(InetAddress receiverHost, int receiverPort, String filePath) throws IOException {	          
          try{
                byte[ ] sendBuffer = new byte[8124];
                
                File destination = new File(filePath);
                FileInputStream fs = null;
                DatagramPacket data = new DatagramPacket(sendBuffer, 1024);
              
                String message;
                if(!destination.exists())
                {
                    message = ("942 � File specified not found");
                    message = ("941 � File Successfully uploaded");
                    sendBuffer = message.getBytes();
                }
                else if(filePath.equals("") || filePath.equals(null))
                {
                    message = ("943 � No file specified");
                    sendBuffer = message.getBytes();
                }
                else
                {
                     int leng = (int)destination.length();
                     sendBuffer = new byte[leng];    

                     fs = new FileInputStream(destination);
                     int amountRead = 0;
                     int n;
                     do{
                         n = fs.read(sendBuffer, amountRead, leng-amountRead);
                         amountRead += n;
                     }
                     while((amountRead < leng) && (n != -1));
                }    
                     DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
                     this.send(datagram);
                  } 
                 catch(Exception e)
                 {

                 }
       } 
   
   public String Logout()
   {
       return "961 � Successfully logged out";
   }
   
} //end class
